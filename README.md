# CSW-TASK

[![N|Solid](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)](https://reactjs.org/) [![N|Solid](https://img.shields.io/badge/Redux-593D88?style=for-the-badge&logo=redux&logoColor=white)](https://react-redux.js.org/) [![N|Solid](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)](https://www.typescriptlang.org/)

[![Build Status](https://gitlab.com/thearavind/csw-task/badges/main/pipeline.svg)](https://gitlab.com/thearavind/csw-task/-/commits/main)

This application has been deployed to GitLab Pages at https://thearavind.gitlab.io/csw-task

## Installation

Install the dependencies and start the dev server.

```sh
yarn install
yarn start
```
