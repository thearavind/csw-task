import auth from "./auth";
import profile from "./profile";
import weather from './weather'

const combinedReducers = {
  auth,
  profile,
  weather
};

export default combinedReducers;