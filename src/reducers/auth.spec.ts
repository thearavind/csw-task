import authReducer, { initialState } from "./auth";
import { LOGIN_SUCCESS, LOGOUT } from "../actions/auth";

const loginResponse = {
  accessToken: "1234",
  expiresIn: String(Date.now()),
};

const dummyState = {
  accessToken: "12345",
  expiresIn: String(Date.now()),
  isLoggedIn: true,
};

describe("state: undefined", () => {
  test("should return the initial state", () => {
    expect(authReducer(undefined, { type: null })).toEqual(initialState);
  });

  test("should return update the login response in state", () => {
    expect(
      authReducer(undefined, { type: LOGIN_SUCCESS, payload: loginResponse })
    ).toEqual({ ...initialState, ...loginResponse, isLoggedIn: true });
  });

  test("should reset the auth state on logout", () => {
    expect(authReducer(undefined, { type: LOGOUT })).toEqual(initialState);
  });
});

describe("state: initialState", () => {
  test("should return the initial state", () => {
    expect(authReducer(initialState, { type: null })).toEqual(initialState);
  });

  test("should return update the login response in state", () => {
    expect(
      authReducer(initialState, { type: LOGIN_SUCCESS, payload: loginResponse })
    ).toEqual({ ...initialState, ...loginResponse, isLoggedIn: true });
  });

  test("should reset the auth state on logout", () => {
    expect(
      authReducer(
        { ...initialState, ...loginResponse, isLoggedIn: true },
        { type: LOGOUT }
      )
    ).toEqual(initialState);
  });
});

describe("state: dummyState", () => {
  test("should return the initial state", () => {
    expect(authReducer(dummyState, { type: null })).toEqual(dummyState);
  });

  test("should return update the login response in state", () => {
    expect(
      authReducer(dummyState, { type: LOGIN_SUCCESS, payload: loginResponse })
    ).toEqual({ ...initialState, ...loginResponse, isLoggedIn: true });
  });

  test("should reset the auth state on logout", () => {
    expect(authReducer(dummyState, { type: LOGOUT })).toEqual(initialState);
  });
});
