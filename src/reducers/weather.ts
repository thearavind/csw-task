import { AnyAction } from "@reduxjs/toolkit";
import {
  WEATHER_QUERY,
  WEATHER_QUERY_SUCCESS,
  WEATHER_QUERY_FAILURE,
  weatherData,
} from "actions/weather";

export interface weatherState {
  isQueryLoading: boolean;
  isQueryError: boolean;
  queryResult?: weatherData;
}

const initialState: weatherState = {
  isQueryLoading: false,
  isQueryError: false,
};

export default function counter(
  state = initialState,
  action: AnyAction
): weatherState {
  switch (action.type) {
    case WEATHER_QUERY:
      return { ...state, isQueryLoading: true };
    case WEATHER_QUERY_SUCCESS:
      return {
        ...state,
        isQueryLoading: false,
        isQueryError: false,
        queryResult: action.payload,
      };
    case WEATHER_QUERY_FAILURE:
      return {
        ...state,
        isQueryLoading: false,
        isQueryError: true,
        queryResult: undefined,
      };
    default:
      return state;
  }
}
