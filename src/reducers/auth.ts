import { AnyAction } from "@reduxjs/toolkit";
import { LOGIN_SUCCESS, LOGOUT } from "../actions/auth";

export interface authState {
  isLoggedIn: boolean;
  accessToken?: string;
  expiresIn?: string;
}
export const initialState: authState = { isLoggedIn: false };

export default function counter(
  state = initialState,
  action: AnyAction
): authState {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return { ...state, ...action.payload, isLoggedIn: true };
    case LOGOUT:
      return initialState;
    default:
      return state;
  }
}
