import { AnyAction } from "@reduxjs/toolkit";
import {
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAIL,
  Profile,
} from "actions/profile";

export interface profileState {
  isProfileLoaded: boolean;
  profileData?: Profile;
}
const initialState: profileState = { isProfileLoaded: false };

export default function counter(
  state = initialState,
  action: AnyAction
): profileState {
  switch (action.type) {
    case FETCH_PROFILE:
      return { ...state, isProfileLoaded: false };
    case FETCH_PROFILE_SUCCESS:
      return { ...state, isProfileLoaded: true, profileData: action.payload };
    case FETCH_PROFILE_FAIL:
      return { ...state, isProfileLoaded: false };
    default:
      return state;
  }
}
