export const GOOGLE_OAUTH_2_BASE_URL = "https://accounts.google.com/o/oauth2/v2/auth";
export const GOOGLE_OAUTH_2_CLIENT_ID = "6916069169-j0hbm68u25egfcdt37ugs60fg2e17ha8.apps.googleusercontent.com";
export const GOOGLE_OAUTH_2_REDIRECT_URL = "https://thearavind.gitlab.io/csw-task/callback";
export const GOOGLE_OAUTH_2_PROFILE_SCOPE = "https://www.googleapis.com/auth/userinfo.profile";
export const GOOGLE_OAUTH_2_EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email"
export const GOOGLE_OAUTH_2_RESPONSE_TYPE = "token";
export const GOOGLE_OAUTH_2_REVOKE_TOKEN_URL = "https://oauth2.googleapis.com/revoke";
export const GOOGLE_GET_PROFILE_DATA_URL = "https://www.googleapis.com/oauth2/v3/userinfo";
export const WEATHER_QUERY_API_BASE_URL = "https://api.openweathermap.org/data/2.5/weather";
export const OPEN_WEATHER_APP_ID = "61812426f1191fc7ceefcc6d226eb3f8"
export const WEATHER_ICON_BASE_URL = "http://openweathermap.org/img/wn/{icon}@2x.png"

export const getGoogleOauthURL = (): string => {
  return (
    `${GOOGLE_OAUTH_2_BASE_URL}?` +
    `client_id=${GOOGLE_OAUTH_2_CLIENT_ID}&` +
    `redirect_uri=${GOOGLE_OAUTH_2_REDIRECT_URL}&` +
    `scope=${GOOGLE_OAUTH_2_PROFILE_SCOPE} ${GOOGLE_OAUTH_2_EMAIL_SCOPE}&` +
    `response_type=${GOOGLE_OAUTH_2_RESPONSE_TYPE}&` +
    `include_granted_scopes=true`
  );
};
