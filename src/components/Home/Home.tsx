import { FC } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "store";
import { profileDataSelector } from "selectors/profile";

import "./Home.css";

const mapState = (state: RootState) => ({
  profileData: profileDataSelector(state),
});

const connector = connect(mapState);

type PropsFromRedux = ConnectedProps<typeof connector>;
interface HomeProps extends PropsFromRedux {}

const Home: FC<HomeProps> = ({ profileData }) => {
  return (
    <div className="home-container">
      <img className="profile-image" src={profileData?.picture} alt="Profile" />
      <span className="profile-name">Welcome {profileData?.name}</span>
      <span className="profile-email">{profileData?.email}</span>
    </div>
  );
};

export default connector(Home);
