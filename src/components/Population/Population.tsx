import { FC } from "react";
import populationData from "utils/population-data";

import "./Population.css";

const headerFields = Object.keys(populationData[0]).map((field) =>
  field.replace(/./, (text) => text.toUpperCase())
);

const Population: FC = () => {
  return (
    <div className="table-container">
      <div className="table-overflow-container">
        <table>
          <tbody>
            <tr>
              {headerFields.map((header, index) => (
                <th key={`${header}_${index}`}>{header}</th>
              ))}
            </tr>
            {populationData.map((data, itemIndex) => {
              return (
                <tr key={`${data.code}_${itemIndex}`}>
                  {Object.values(data).map((fieldValue, fieldIndex) => (
                    <td key={`${data.code}_${itemIndex}_${fieldIndex}`}>
                      {fieldValue}
                    </td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Population;
