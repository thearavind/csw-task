import { FC } from "react";

import "./NotFound.css";

const NotFound: FC = () => {
  return (
    <div className="not-found-container">
      <h1>404</h1>
      <h2>No matching route found</h2>
    </div>
  );
};

export default NotFound;
