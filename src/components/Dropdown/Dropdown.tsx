import { FC } from "react";
import "./Dropdown.css";

interface DropdownProps {
  buttonText: string;
}

const Dropdown: FC<DropdownProps> = ({ children, buttonText }) => {
  return (
    <div className="dropdown">
      <button className="dropdown-button">{buttonText}</button>
      <div className="dropdown-content">{children}</div>
    </div>
  );
};

export default Dropdown;
