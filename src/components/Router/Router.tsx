import { FC, useEffect } from "react";
import {
  Switch,
  Route,
  Redirect,
  RouteComponentProps,
  withRouter,
} from "react-router-dom";
import Login from "../Login/Login";
import LoginSuccess from "../LoginSuccess/LoginSuccess";
import PrivateRoute from "components/PrivateRoute/PrivateRoute";
import Home from "components/Home/Home";
import IdleTimeout from "components/IdleTimeout/IdleTimeout";
import Population from "components/Population/Population";
import Search from "components/Weather/Weather";
import NotFound from "components/NotFound/NotFound";
import { logOut } from "actions/auth";
import { accessToken, isTokenValid } from "selectors/auth";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "store";

const mapState = (state: RootState) => ({
  isAuthenticated: isTokenValid(state),
  accessToken: accessToken(state),
});

const mapDispatch = {
  logOut: (accessToken: string) => logOut(accessToken),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface RouterProps extends PropsFromRedux, RouteComponentProps {}

const Router: FC<RouterProps> = ({
  location,
  isAuthenticated,
  accessToken,
  logOut,
  history
}) => {
  useEffect(() => {
    if (!isAuthenticated && accessToken) {
      localStorage.clear();
      logOut(accessToken as string);
      history.push("/login")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  return (
    <IdleTimeout IdleTimeInSeconds={59} PromptTimeInSeconds={1}>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/callback" component={LoginSuccess} />
        <PrivateRoute exact path="/home" component={Home} />
        <PrivateRoute exact path="/population" component={Population} />
        <PrivateRoute exact path="/weather" component={Search} />
        <Redirect exact from="/" to="/login" />
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </IdleTimeout>
  );
};

export default connector(withRouter(Router));
