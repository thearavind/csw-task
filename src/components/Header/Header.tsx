import { FC } from "react";
import { Link, useHistory } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { isLoggedIn, accessToken } from "selectors/auth";
import { logOut } from "actions/auth";
import { profileDataSelector } from "selectors/profile";
import { RootState } from "store";
import Logo from "assets/logo.png";

import "./Header.css";
import Dropdown from "components/Dropdown/Dropdown";

const mapState = (state: RootState) => ({
  isAuthenticated: isLoggedIn(state),
  profileData: profileDataSelector(state),
  accessToken: accessToken(state),
});

const mapDispatch = {
  logOut: (accessToken: string) => logOut(accessToken),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
interface HeaderProps extends PropsFromRedux {}

const Header: FC<HeaderProps> = ({
  isAuthenticated,
  profileData,
  accessToken,
  logOut,
}) => {
  const history = useHistory();
  const handleLogOut = () => {
    localStorage.clear();
    logOut(String(accessToken));
    history.push("/login");
  };

  return (
    <div className="header">
      <div className="header-container">
        <div className="logo-container">
          <Link to={`${isAuthenticated ? "/home" : "/login"}`}>
            <img src={Logo} alt="Company logo" />
          </Link>
        </div>
        {isAuthenticated && (
          <>
            <div className="links-container">
              <Link to="/population">Population</Link>
              <Link to="/weather">Weather</Link>
            </div>
            <div className="dropdown-container">
              <Dropdown buttonText={profileData?.name as string}>
                <span onClick={handleLogOut}>Log Out</span>
                <span className="dropdown-for-sm">
                  <Link to="/population">Population</Link>
                </span>
                <span className="dropdown-for-sm">
                  <Link to="/weather">Weather</Link>
                </span>
              </Dropdown>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default connector(Header);
