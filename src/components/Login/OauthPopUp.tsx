import { FC, ReactChild } from "react";

interface PopUpProps {
  width: number;
  height: number;
  url: string;
  title: string;
  onClose: () => any;
  children?: ReactChild;
}

const OauthPopUp: FC<PopUpProps> = ({
  url,
  title,
  width,
  height,
  onClose,
  children,
}) => {
  const openPopUp = () => {
    const left = window.screenX + (window.outerWidth - width) / 2;
    const top = window.screenY + (window.outerHeight - height) / 2.5;

    const windowFeatures = `toolbar=0,scrollbars=1,status=1,resizable=0,location=1,menuBar=0,width=${width},height=${height},top=${top},left=${left}`;

    const externalWindow = window.open(url, title, windowFeatures);

    const timer = setInterval(function () {
      if (externalWindow?.closed) {
        onClose();
        clearInterval(timer);
      }
    }, 500);
  };

  return <div onClick={openPopUp}>{children}</div>;
};

export default OauthPopUp;
