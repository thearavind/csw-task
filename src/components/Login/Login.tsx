import { FC, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { loginSucess } from "actions/auth";
import { fetchProfileData } from "actions/profile";
import { getGoogleOauthURL } from "utils/app-constants";
import OauthPopUp from "./OauthPopUp";

import "./Login.css";

export interface LocationState {
  from: From;
}

export interface From {
  pathname: string;
}

const DEFAULT_LOCATION_STATE: LocationState = { from: { pathname: "/home" } };

const mapDispatch = {
  publishLoginSuccess: (payload: object) => loginSucess(payload),
  getProfileData: (accessToken: string) => fetchProfileData(accessToken),
};

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
interface LoginProps
  extends PropsFromRedux,
    RouteComponentProps<object, object, LocationState> {}

const Login: FC<LoginProps> = ({
  publishLoginSuccess,
  getProfileData,
  history,
  location,
}) => {
  const { from } = location.state || DEFAULT_LOCATION_STATE;

  useEffect(() => {
    const accessToken = localStorage.getItem("access_token");
    const expiresIn = localStorage.getItem("expires_in");

    if (expiresIn && accessToken && Number(expiresIn) > Date.now()) {
      processLoginData(accessToken as string, expiresIn);
      history.replace(from);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const processLoginData = (accessToken: string, expiresIn: string) => {
    publishLoginSuccess({ accessToken, expiresIn });
    getProfileData(String(accessToken));
  };

  const onPopUpClose = () => {
    const accessToken = localStorage.getItem("access_token");
    const expiresIn = localStorage.getItem("expires_in");

    if (accessToken && expiresIn) {
      processLoginData(accessToken as string, expiresIn);
      history.push("home");
    }
  };

  return (
    <div className="login-container">
      <div className="login-card">
        <span className="login-instruction">Please login to continue</span>
        <OauthPopUp
          url={getGoogleOauthURL()}
          onClose={onPopUpClose}
          title="Test"
          width={500}
          height={500}
        >
          <button type="button" className="google-button">
            Sign in with Google
          </button>
        </OauthPopUp>
      </div>
    </div>
  );
};

export default connector(withRouter(Login));
