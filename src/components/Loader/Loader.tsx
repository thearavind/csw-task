import { FC } from "react";
import "./Loader.css";

const Loader: FC = () => {
  return (
    <div className="spin-loader">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Loader;
