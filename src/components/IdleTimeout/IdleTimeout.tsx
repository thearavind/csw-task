import { logOut } from "actions/auth";
import { Component, ReactNode } from "react";
import { connect, ConnectedProps } from "react-redux";
import Modal from "components/Modal/Modal";
import { RootState } from "store";
import { accessToken, isLoggedIn } from "selectors/auth";
import { RouteComponentProps, withRouter } from "react-router-dom";

const mapState = (state: RootState) => ({
  isAuthenticated: isLoggedIn(state),
  accessToken: accessToken(state),
});

const mapDispatch = {
  logOut: (accessToken: string) => logOut(accessToken),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface IdleTimeoutProps extends PropsFromRedux, RouteComponentProps {
  IdleTimeInSeconds: number;
  PromptTimeInSeconds: number;
  children?: ReactNode;
}

interface IdleTimeoutState {
  showPrompt: boolean;
}

class IdleTimeout extends Component<IdleTimeoutProps, IdleTimeoutState> {
  idleTimer!: NodeJS.Timeout;
  logOutTimer!: NodeJS.Timeout;

  constructor(props: IdleTimeoutProps) {
    super(props);

    this.state = {
      showPrompt: false,
    };

    if (props.isAuthenticated) this.addIdleListeners();
  }

  componentDidUpdate(prevProps: IdleTimeoutProps) {
    if (!prevProps.isAuthenticated && this.props.isAuthenticated) {
      this.addIdleListeners();
    } else if (prevProps.isAuthenticated && !this.props.isAuthenticated) {
      clearTimeout(this.idleTimer);
      clearTimeout(this.logOutTimer);
      this.removeIdleListeners();
      this.setState({ showPrompt: false });
    }
  }

  addIdleListeners = (): void => {
    window.onload = this.resetIdleTimer;
    window.onmousemove = this.resetIdleTimer;
    window.onmousedown = this.resetIdleTimer;
    window.ontouchstart = this.resetIdleTimer;
    window.onclick = this.resetIdleTimer;
    window.onkeydown = this.resetIdleTimer;
    window.onscroll = this.resetIdleTimer;
    this.resetIdleTimer();
  };

  removeIdleListeners = (): void => {
    window.onload = null;
    window.onmousemove = null;
    window.onmousedown = null;
    window.ontouchstart = null;
    window.onclick = null;
    window.onkeydown = null;
    window.onscroll = null;
  };

  resetIdleTimer = (): void => {
    const { showPrompt } = this.state;
    const { IdleTimeInSeconds } = this.props;

    clearTimeout(this.idleTimer);
    if (!showPrompt) {
      this.idleTimer = setTimeout(this.promptUserInput, IdleTimeInSeconds * 1000);
    }
  };

  promptUserInput = (): void => {
    this.setState({ showPrompt: true }, () => {
      this.resetIdleTimer();
      this.removeIdleListeners();
      this.startLogOutTimer();
    });
  };

  startLogOutTimer = (): void => {
    const { showPrompt } = this.state;
    const { PromptTimeInSeconds } = this.props;

    if (showPrompt) {
      this.logOutTimer = setTimeout(this.LogOut, PromptTimeInSeconds * 1000);
    }
  };

  clearLogOutTimer = (): void => {
    if (this.logOutTimer) {
      this.setState({ showPrompt: false }, () => {
        clearTimeout(this.logOutTimer);
        this.addIdleListeners();
      });
    }
  };

  LogOut = (): void => {
    const { history, accessToken, logOut } = this.props;
    this.setState({ showPrompt: false });
    localStorage.clear();
    logOut(String(accessToken));
    history.push("/login");
  };

  render() {
    const { isAuthenticated, children } = this.props;
    const { showPrompt } = this.state;

    if (isAuthenticated && showPrompt) {
      return (
        <>
          <Modal
            content="Do you want to continue ?"
            buttonText="Continue"
            onAction={this.clearLogOutTimer}
          />
          {children}
        </>
      );
    }

    return <>{children}</>;
  }
}

export default connector(withRouter(IdleTimeout));
