import { fireEvent, render, screen } from "@testing-library/react";
import Modal from "./Modal";

const TEST_CONTENT = "Pls login!";
const TEST_BUTTON = "Continue";
const TEST_ACTION = jest.fn();

test("Matches the snapshot", () => {
  const container = render(
    <Modal
      content={TEST_CONTENT}
      buttonText={TEST_BUTTON}
      onAction={TEST_ACTION}
    />
  );
  expect(container).toMatchSnapshot();
});

test("renders action button with correct text", () => {
  render(
    <Modal
      content={TEST_CONTENT}
      buttonText={TEST_BUTTON}
      onAction={TEST_ACTION}
    />
  );

  const linkElement = screen.getByText(TEST_BUTTON);
  expect(linkElement).not.toBeNull();
});

test("renders modal content with correct text", () => {
  render(
    <Modal
      content={TEST_CONTENT}
      buttonText={TEST_BUTTON}
      onAction={TEST_ACTION}
    />
  );

  const linkElement = screen.getByText(TEST_CONTENT);
  expect(linkElement).not.toBeNull();
});

test("onAction callback is called on action button click", () => {
  render(
    <Modal
      content={TEST_CONTENT}
      buttonText={TEST_BUTTON}
      onAction={TEST_ACTION}
    />
  );

  const linkElement = screen.getByText(TEST_BUTTON);
  fireEvent(
    linkElement,
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
    })
  );
  expect(TEST_ACTION).toBeCalledTimes(1);
});
