import { FC } from "react";
import "./Modal.css";

interface ModalProps {
  content: string;
  buttonText: string;
  onAction: () => void;
}

const TimeOutModal: FC<ModalProps> = ({ content, buttonText, onAction }) => {
  return (
    <div className="modal-backdrop">
      <div className="modal-body">
        <span className="modal-content">{content}</span>
        <button className="modal-action" onClick={onAction}>
          {buttonText}
        </button>
      </div>
    </div>
  );
};

export default TimeOutModal;
