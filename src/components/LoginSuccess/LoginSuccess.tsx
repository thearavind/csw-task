import { FC, useEffect } from "react";

const LoginSuccess: FC = () => {
  useEffect(() => {
    const urlParams = new URLSearchParams(
      window.location.hash.replace("#", "?")
    );
    const access_token = urlParams.get("access_token");
    const expiresIn = urlParams.get("expires_in");
    const expiresAt = Date.now() + Number(expiresIn) * 1000;
    if (access_token) {
      localStorage.setItem("access_token", access_token);
      localStorage.setItem("expires_in", String(expiresAt));
    }
    setTimeout(() => {
      window.close();
    });
  }, []);

  return <></>;
};

export default LoginSuccess;
