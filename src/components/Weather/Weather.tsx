import { ChangeEvent, FC, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import {
  weatherQueryLoadingSelector,
  weatherQueryResultSelector,
  weatherQueryErrorSelector,
} from "selectors/weather";
import { RootState } from "store";
import { queryWeather } from "actions/weather";
import SearchIcon from "assets/search_icon.png";

import "./Weather.css";
import { WEATHER_ICON_BASE_URL } from "utils/app-constants";
import Loader from "components/Loader/Loader";

const mapState = (state: RootState) => ({
  isLoading: weatherQueryLoadingSelector(state),
  queryResult: weatherQueryResultSelector(state),
  isError: weatherQueryErrorSelector(state),
});

const mapDispatch = {
  startQuery: (query: string) => queryWeather(query),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;
interface SearchProps extends PropsFromRedux {}

const Search: FC<SearchProps> = ({
  isLoading,
  queryResult,
  isError,
  startQuery,
}) => {
  const [query, setQuery] = useState<string>("");

  const handleQueryChange = (e: ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
  };

  const handleSearch = () => {
    startQuery(query);
  };

  const getIconURL = (icon: string): string => {
    return WEATHER_ICON_BASE_URL.replace("{icon}", icon);
  };

  return (
    <>
      <div className="search-container">
        <div className="search-box">
          <input
            value={query}
            onChange={handleQueryChange}
            type="text"
            placeholder="Search.."
            name="search"
          />
          <button onClick={handleSearch} disabled={isLoading} type="submit">
            <img src={SearchIcon} alt="search" />
          </button>
        </div>
      </div>
      {
        isLoading && <Loader />
      }
      {isError && (
        <div className="error-container">
          <span>
            Something went wrong please check if the city name is valid
          </span>
        </div>
      )}
      <div className="search-result">
        {queryResult && !isError && !isLoading && (
          <>
            <div className="search-result-card">
              <div className="weather-card-header">
                <div>
                  <span className="city-name">{queryResult?.name}</span>
                </div>
                <div className="weather-icon-container">
                  <img
                    src={getIconURL(queryResult?.weather[0]?.icon as string)}
                    alt="weather"
                  />
                  <span>{queryResult?.weather[0]?.description}</span>
                </div>
              </div>
              <div className="weather-details">
                <span>Temp: {queryResult?.main?.temp} C</span>
                <span>Feels like: {queryResult?.main?.feels_like} C</span>
                <span>Humidity: {queryResult?.main?.humidity} %</span>
                <span>Pressure: {queryResult?.main?.pressure} hPa</span>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default connector(Search);
