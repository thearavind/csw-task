import { Dispatch } from "@reduxjs/toolkit";
import {
  OPEN_WEATHER_APP_ID,
  WEATHER_QUERY_API_BASE_URL,
} from "utils/app-constants";

export const WEATHER_QUERY = "city/query";
export const WEATHER_QUERY_SUCCESS = "city/query/success";
export const WEATHER_QUERY_FAILURE = "city/query/failure";

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Main {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

export interface weatherData {
  weather: Weather[];
  base: string;
  main: Main;
  visibility: number;
  dt: number;
  timezone: number;
  id: number;
  name: string;
  cod: number;
}

export const queryWeather = (query: string) => (dispatch: Dispatch) => {
  dispatch({ type: WEATHER_QUERY });
  fetch(
    `${WEATHER_QUERY_API_BASE_URL}?q=${query}&units=metric&appid=${OPEN_WEATHER_APP_ID}`
  )
    .then((response) =>
      response.status === 200 ? response.json() : Promise.reject(null)
    )
    .then((cityData) =>
      dispatch({ type: WEATHER_QUERY_SUCCESS, payload: cityData })
    )
    .catch((e) => {
      console.error("Weather query API throwed", e);
      dispatch({ type: WEATHER_QUERY_FAILURE });
    });
};
