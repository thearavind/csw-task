import { GOOGLE_GET_PROFILE_DATA_URL } from "utils/app-constants";
import { Dispatch } from "@reduxjs/toolkit";

export const FETCH_PROFILE = "fetch/profile";
export const FETCH_PROFILE_SUCCESS = "fetch/profile/success";
export const FETCH_PROFILE_FAIL = "fetch/profile/fail";

export interface Profile {
  sub: string;
  name: string;
  given_name: string;
  family_name: string;
  picture: string;
  email: string;
  email_verified: boolean;
  locale: string;
}

export const fetchProfileData =
  (accessToken: string) => (dispatch: Dispatch) => {
    fetch(`${GOOGLE_GET_PROFILE_DATA_URL}?access_token=${accessToken}`)
      .then((response) => response.json())
      .then((payload: Profile) => {
        console.log(payload);
        dispatch({ type: FETCH_PROFILE_SUCCESS, payload });
      })
      .catch((e) => {
        dispatch({ type: FETCH_PROFILE_FAIL });
      });
  };
