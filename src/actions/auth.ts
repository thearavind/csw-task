import { Dispatch } from "@reduxjs/toolkit";
import { GOOGLE_OAUTH_2_REVOKE_TOKEN_URL } from "utils/app-constants";

export const LOGIN_SUCCESS = "login/success";
export const LOGOUT = "logout";

export function loginSucess(payload: object) {
  return {
    type: LOGIN_SUCCESS,
    payload,
  };
}

export const logOut = (accessToken: string) => (dispatch: Dispatch) => {
  fetch(`${GOOGLE_OAUTH_2_REVOKE_TOKEN_URL}?token=${accessToken}`, {
    method: "POST",
    headers: { "Content-type": "application/x-www-form-urlencoded" },
  })
    .then((response) => response.json())
    .then(() => dispatch({ type: LOGOUT }))
    .catch(() => dispatch({ type: LOGOUT }));
};
