import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import Header from "./components/Header/Header";
import Router from "./components/Router/Router";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <Header />
        <Router />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
