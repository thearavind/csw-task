import { isTokenValid, expiresIn } from "./auth";

const authState = {
  isLoggedIn: false,
  accessToken: "abcd",
  expiresIn: String(Date.now()),
};

test("Returns the token expiry time", () => {
  const result = expiresIn.resultFunc(authState);
  expect(result).toEqual(authState.expiresIn);
});

test("Returns token is valid", () => {
  const result = isTokenValid.resultFunc(
    String(Date.now() + 3000),
    authState.accessToken
  );
  expect(result).toEqual(true);
});

test("Expired return token is not valid", () => {
  const result = isTokenValid.resultFunc(
    String(Date.now() - 3000),
    authState.accessToken
  );
  expect(result).toEqual(false);
});

test("No access token returns not authenticated", () => {
  const result = isTokenValid.resultFunc(String(Date.now() - 3000), null);
  expect(result).toEqual(false);
});
