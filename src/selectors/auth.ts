import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "store";
import { authState } from "reducers/auth";

const authSelector = (state: RootState): authState => state.auth;

export const isLoggedIn = createSelector(
  authSelector,
  (auth) => auth.isLoggedIn
);

export const expiresIn = createSelector(authSelector, (auth) => auth.expiresIn);

export const accessToken = createSelector(
  authSelector,
  (auth) => auth.accessToken
);

export const isTokenValid = createSelector(
  expiresIn,
  accessToken,
  (expiresIn, accessToken) =>
    !!expiresIn && !!accessToken && Number(expiresIn) > Date.now()
);
