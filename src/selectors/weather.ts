import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "store";
import { weatherState } from "reducers/weather";

export const weatherSelector = (state: RootState): weatherState =>
  state.weather;

export const weatherQueryResultSelector = createSelector(
  weatherSelector,
  (city) => city?.queryResult
);

export const weatherQueryErrorSelector = createSelector(
  weatherSelector,
  (city) => city?.isQueryError
);

export const weatherQueryLoadingSelector = createSelector(
  weatherSelector,
  (city) => city?.isQueryLoading
);
