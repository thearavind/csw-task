import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "store";
import { profileState } from "reducers/profile";

export const profileSelector = (state: RootState): profileState =>
  state.profile;

export const profileDataSelector = createSelector(
  profileSelector,
  (profile) => profile?.profileData
);
